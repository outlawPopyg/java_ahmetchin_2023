drop table if exists student;
drop table if exists student_course;
drop table if exists course;
drop table if exists lesson;

-- создание таблицы
create table student
(
    id         bigserial primary key, -- идентификатор строки - всегда уникальный
    first_name varchar(20),
    last_name  varchar(20),
    age        integer check ( age > 18 and age < 120)
);

create table course
(
    id          serial primary key,
    title       varchar(20),
    description varchar(100),
    start_date  date,
    finish_date date
);

create table lesson
(
    id          serial primary key,
    name        varchar(20),
    start_time  time,
    finish_time time,
    day_of_week varchar(20)
);

-- Многие-ко-многим (Many-To-Many) - например, у студента есть много курсов, а у курса есть много студентов. Нужно создать третью таблицу с двумя внешними ключами.
create table student_course (
    course_id bigint,
    student_id bigint,
    foreign key (course_id) references course(id),
    foreign key (student_id) references student(id)
);

-- изменение таблицы (добавление внешнего ключа)
-- Один-ко-многим (One-To-Many) - в курсе есть много уроков. Нужно у урока сделать внешний ключ, ссылающийся на id-курса.
alter table lesson
    add course_id bigint;
alter table lesson
    add foreign key (course_id) references course (id);

alter table student
    add phone_number varchar(20) not null default '';

alter table student add email varchar(20) unique;
alter table student add password varchar(20);

