update student
set phone_number = '79377777777',
    age          = 24
where id = 2;


insert into student(first_name, last_name, age)
values ('Марсель', 'Сидиков', 28);
insert into student(first_name, last_name, age)
values ('Айрат', 'Мухутдинов', 23);
insert into student(first_name, last_name, age)
values ('Виктор', 'Евлампьев', 23);
insert into student(first_name, last_name, age)
values ('Максим', 'Поздеев', 21);

insert into course (title, description, start_date, finish_date)
values ('Java', 'Изучаем разработку на Java', '2020-02-02', '2021-02-02'),
       ('Python', 'Изучаем разработку на Python', '2020-03-03', '2021-03-03'),
       ('.NET', 'Изучаем разработку на .NET', '2020-04-04', '2021-04-04'),
       ('SQL', 'Изучаем SQL', '2020-05-05', '2021-05-05');

UPDATE lesson SET name = 'Generics', start_time = '12:00:00', finish_time = '13:00:00', day_of_week = 'MONDAY', course_id = 1 WHERE id = 1;
UPDATE lesson SET name = 'OOP', start_time = '13:00:00', finish_time = '14:00:00', day_of_week = 'FRIDAY', course_id = 1 WHERE id = 2;
UPDATE lesson SET name = 'Arrays', start_time = '14:00:00', finish_time = '15:00:00', day_of_week = 'SUNDAY', course_id = 2 WHERE id = 3;
UPDATE lesson SET name = 'OOP', start_time = '15:00:00', finish_time = '16:00:00', day_of_week = 'FRIDAY', course_id = 2 WHERE id = 4;
UPDATE lesson SET name = 'ASP', start_time = '16:00:00', finish_time = '17:00:00', day_of_week = 'SUNDAY', course_id = 3 WHERE id = 5;
UPDATE lesson SET name = 'OOP', start_time = '16:00:00', finish_time = '17:00:00', day_of_week = 'MONDAY', course_id = 3 WHERE id = 6;
UPDATE lesson SET name = 'Tables', start_time = '17:00:00', finish_time = '18:00:00', day_of_week = 'FRIDAY', course_id = 4 WHERE id = 7;
UPDATE lesson SET name = 'Queries', start_time = '18:00:00', finish_time = '19:00:00', day_of_week = 'SUNDAY', course_id = 4 WHERE id = 8;


UPDATE student_course SET course_id = 1, student_id = 1 WHERE ctid = '(0,1)';
UPDATE student_course SET course_id = 2, student_id = 1 WHERE ctid = '(0,2)';
UPDATE student_course SET course_id = 2, student_id = 2 WHERE ctid = '(0,3)';
UPDATE student_course SET course_id = 3, student_id = 2 WHERE ctid = '(0,4)';
UPDATE student_course SET course_id = 4, student_id = 3 WHERE ctid = '(0,5)';
UPDATE student_course SET course_id = 3, student_id = 3 WHERE ctid = '(0,6)';
UPDATE student_course SET course_id = 1, student_id = 4 WHERE ctid = '(0,7)';

