package jdbc;

import jdk.jshell.spi.ExecutionControl;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class SimpleDataSource implements DataSource {

    private Connection connection;
    private final String url;
    private final String userName;
    private final String password;

    public SimpleDataSource(String url, String userName, String password) {
        this.url = url;
        this.userName = userName;
        this.password = password;

        openConnection();
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (this.connection == null || this.connection.isClosed()) {
            openConnection();
        }
        return connection;
    }

    private void openConnection() {
        try {
            this.connection = DriverManager.getConnection(url, userName, password);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        try {
            throw new ExecutionControl.NotImplementedException("");
        } catch (ExecutionControl.NotImplementedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        try {
            throw new ExecutionControl.NotImplementedException("");
        } catch (ExecutionControl.NotImplementedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        try {
            throw new ExecutionControl.NotImplementedException("");
        } catch (ExecutionControl.NotImplementedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        try {
            throw new ExecutionControl.NotImplementedException("");
        } catch (ExecutionControl.NotImplementedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        try {
            throw new ExecutionControl.NotImplementedException("");
        } catch (ExecutionControl.NotImplementedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        try {
            throw new ExecutionControl.NotImplementedException("");
        } catch (ExecutionControl.NotImplementedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        try {
            throw new ExecutionControl.NotImplementedException("");
        } catch (ExecutionControl.NotImplementedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        try {
            throw new ExecutionControl.NotImplementedException("");
        } catch (ExecutionControl.NotImplementedException e) {
            throw new RuntimeException(e);
        }
    }
}
