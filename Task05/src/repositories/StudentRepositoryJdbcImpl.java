package repositories;

import models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public class StudentRepositoryJdbcImpl implements StudentRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL_USERS = "select * from student;";

    // language=SQL
    private static final String SQL_INSERT_USER = "insert into student(first_name, last_name, email, password) " + "values (?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_GET_BY_ID = "select * from student where id = ?";

    // language=SQL
    private static final String SQL_UPDATE_STUDENT =
            "update student set first_name = ?, last_name = ?, age = ?, phone_number = ?, email = ?, password = ? where id = ?;";

    // language=SQL
    private static final String SQL_DELETE_STUDENT = "delete from student where id = ?;";

    // language=SQL
    private static final String SQL_FIND_ALL_BY_AGE_GREATER =
            "select * from student where age > ? order by id desc;";

    // language=SQL
    private static final String SQL_DELETE_FROM_STUDENT_COURSE =
            "delete from student_course where student_id = ?;";

    private static final Function<ResultSet, Student> resultSetMapper = row -> {
        try {
            Long id = row.getLong("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            Integer age = row.getObject("age", Integer.class);
            return new Student(id, firstName, lastName, age);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };
    private final DataSource dataSource;

    public StudentRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Student> findAll() {

        List<Student> students = new LinkedList<>();

        try (Connection connection = dataSource.getConnection(); Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_USERS)) {
                while (resultSet.next()) {
                    students.add(resultSetMapper.apply(resultSet));
                }
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return students;
    }

    @Override
    public void save(Student student) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, student.getFirstName());
            statement.setString(2, student.getLastName());
            statement.setString(3, student.getEmail());
            statement.setString(4, student.getPassword());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't save user");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                student.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException();
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Optional<Student> findById(Long id) {
        try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_GET_BY_ID)) {

            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Student student = resultSetMapper.apply(resultSet);
                    return Optional.of(student);
                } else {
                    return Optional.empty();
                }
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void update(Student student) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_STUDENT)) {

            System.out.println(student.getPhoneNumber());

            statement.setString(1, student.getFirstName());
            statement.setString(2, student.getLastName());
            statement.setInt(3, student.getAge());
            statement.setString(4, student.getPhoneNumber() != null ? student.getPhoneNumber() : "");
            statement.setString(5, student.getEmail());
            statement.setString(6, student.getPassword());
            statement.setLong(7, student.getId());

            int affectedRow = statement.executeUpdate();

            if (affectedRow != 1) {
                throw new SQLException();
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void delete(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement1 = connection.prepareStatement(SQL_DELETE_STUDENT);
             PreparedStatement statement2 = connection.prepareStatement(SQL_DELETE_FROM_STUDENT_COURSE)) {

            statement2.setLong(1, id);
            statement2.executeUpdate();

            statement1.setLong(1, id);

            int affectedRow = statement1.executeUpdate();

            if (affectedRow != 1) {
                throw new SQLException();
            }

        } catch (SQLException e) {
            System.out.println(e);
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<Student> findAllByAgeGreaterThanOrderByIdDesc(Integer minAge) {
        List<Student> students = new LinkedList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_BY_AGE_GREATER)) {

            statement.setInt(1, minAge);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    students.add(resultSetMapper.apply(resultSet));
                }
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }

        return students;
    }
}
