package services;

import dto.StudentSignUp;
import models.Student;
import repositories.StudentRepository;

public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public void signUp(StudentSignUp form) {
        Student student = new Student(form.getFirstName(), form.getLastName(), form.getEmail(), form.getPassword());
        studentRepository.save(student);
    }
}
