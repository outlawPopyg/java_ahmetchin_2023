import dto.StudentSignUp;
import jdbc.SimpleDataSource;
import models.Student;
import repositories.StudentRepository;
import repositories.StudentRepositoryJdbcImpl;
import services.StudentService;
import services.StudentServiceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(properties.getProperty("db.url"), properties.getProperty("db.username"), properties.getProperty("db.password"));
        StudentRepository studentRepository = new StudentRepositoryJdbcImpl(dataSource);

        StudentService studentService = new StudentServiceImpl(studentRepository);

        Student student1 =
                new Student(1L, "Клим", "Брылин", 19, "kalim@mail.ru", "bakirbagarm339" );

        studentRepository.save(student1);

        studentRepository.delete(2L);

        System.out.println(studentRepository.findAllByAgeGreaterThanOrderByIdDesc(21));
    }
}
