package ru.itis.repositories;

import ru.itis.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository {
    List<Product> findAll();
    void save(Product product);
    Optional<Product> findById(Long id);
    void update(Product product);
    void delete(Long id);
    List<Product> findAllByWeightGreaterThanOrderByIdDesc(int minWeight);
}
