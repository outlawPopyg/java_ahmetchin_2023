package ru.itis.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.repositories.ProductsRepositoryJdbcTemplateImpl;
import ru.itis.services.ProductService;
import ru.itis.services.impl.ProductServiceImpl;
import ru.itis.ui.UI;

import java.io.IOException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        ProductService service = context.getBean("productService", ProductService.class);

        UI ui = new UI(service);
        ui.start();
    }
}
