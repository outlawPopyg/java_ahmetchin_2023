package ru.itis.ui;

import ru.itis.models.Product;
import ru.itis.services.ProductService;

import java.util.Scanner;

public class UI {
    private final Scanner scanner = new Scanner(System.in);

    private final ProductService productService;

    public UI(ProductService productService) {
        this.productService = productService;
    }


    public void start() {
        while (true) {
            printMainMenu();

            String command = scanner.nextLine();

            switch (command) {
                case "1":
                    String title = scanner.nextLine();
                    Integer count = Integer.parseInt(scanner.nextLine());
                    String color = scanner.nextLine();
                    Integer weight = Integer.parseInt(scanner.nextLine());

                    Product product = Product.builder()
                            .title(title)
                            .count(count)
                            .color(color)
                            .weight(weight).build();

                    if (this.productService.addProduct(product)) {
                        System.out.println("Товар добавлен");
                    } else {
                        System.out.println("Возникли проблемы с удаленем товара");
                    }
                    break;

                case "2":
                    System.out.println(productService.getAllProducts());
                    break;

                case "3":
                    Long id = Long.parseLong(scanner.nextLine());
                    if (productService.deleteProduct(id)) {
                        System.out.println("Товар был успешно удален");
                    } else {
                        System.out.println("Возникли проблемы с удалением товара");
                    }
                    break;

                case "4":
                    System.exit(0);
                    break;
                default:
                    System.out.println("Команда не распознана");
            }
        }
    }

    private void printMainMenu() {
        System.out.println("Выберите действие:");
        System.out.println("1. Добавить товар");
        System.out.println("2. Посмотреть список товаров");
        System.out.println("3. Удалить товар по id");
        System.out.println("4. Выход");
    }

}
