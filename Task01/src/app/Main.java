package app;

import dto.SignUpForm;
import mappers.Mappers;
import models.User;
import repositories.UsersRepository;
import repositories.UsersRepositoryFilesImpl;
import services.UsersService;
import services.UsersServiceImpl;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class Main {
    public static void main(String[] args) throws IOException {

        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, form -> Mappers.fromSignUpForm(form));

        usersService.signUp(new SignUpForm("Kalim", "Ahemtchin", "kalim.ahmetchin2003@gmail.com"));
        usersService.signUp(new SignUpForm("John", "Marston", "john@gmail.com"));
        usersService.signUp(new SignUpForm("Dutch", "Van der Linde", "dutch@gmail.com"));

        System.out.println(usersRepository.findAll());

        usersRepository.deleteById("43ab5c8f-d65b-494e-9b77-278e1c5977f1");

        User user = new User("3c0c6b28-a339-4713-9738-94557c4b0841", "John", "Marston","john@gmail.com");
        usersRepository.delete(user);

        User user1 = new User("f3ff72ae-9b9e-461c-aaa3-77f8f2b648c9", "Kalim", "Ahmetchin", "new_email@gmail.com");
        usersRepository.update(user1);


    }
}
