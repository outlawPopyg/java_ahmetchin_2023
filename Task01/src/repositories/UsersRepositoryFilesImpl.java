package repositories;

import mappers.Mappers;
import models.User;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class UsersRepositoryFilesImpl implements UsersRepository {
    private final String fileName;
    private List<User> users;
    private static final Function<User, String> userStringFunction = user ->
            user.getId() + "|" + user.getFirstName() + "|" + user.getLastName() + "|" + user.getEmail();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
        users = new LinkedList<>();
    }

    private void updateList() {
        try {
            this.users = Files.readAllLines(Path.of(this.fileName))
                    .stream().map(Mappers::fromStringToUser).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private User getUserById(String id) {
        return users.stream().filter(u -> Objects.equals(u.getId(), id)).findFirst().orElse(null);
    }

    private void refresh() {
        try {
            new FileWriter(fileName, false).close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        try (Writer writer = new FileWriter(this.fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            for (User entity : users) {
                String userAsString = userStringFunction.apply(entity);
                bufferedWriter.write(userAsString);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> findAll() {
        updateList();
        return users;
    }

    @Override
    public void save(User entity) {
        String id = UUID.randomUUID().toString();
        entity.setId(id);

        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userStringFunction.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        updateList();
        this.users = this.users.stream()
                .map(user -> Objects.equals(user.getId(), entity.getId()) ? entity : user)
                .collect(Collectors.toList());
        refresh();
    }

    @Override
    public void delete(User entity) {
        updateList();
        this.users = this.users.stream().filter(user -> !user.equals(entity)).collect(Collectors.toList());
        refresh();
    }

    @Override
    public void deleteById(String id) {
        updateList();
        User user = getUserById(id);
        users.remove(user);
        refresh();
    }
}
