package repositories;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface CrudRepository<ID, T> {
    List<T> findAll() throws IOException;

    void save(T entity);

    void update(T entity);

    void delete(T entity);

    void deleteById(ID id);

}
