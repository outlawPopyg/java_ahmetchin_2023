package mappers;

import dto.SignUpForm;
import models.User;

public class Mappers {
    public static User fromSignUpForm(SignUpForm form) {
        return new User(form.getFirstName(), form.getLastName(), form.getEmail());
    }

    public static User fromStringToUser(String line) {
        String[] split = line.split("\\|");
        return new User(split[0], split[1], split[2], split[3]);
    }
}
