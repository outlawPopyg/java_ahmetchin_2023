-- Получить названия курсов, на которые записан Марсель

select c.title as course_title from course c where c.id in (
    select sc.course_id from student_course sc where sc.student_id in (select s.id from student s where s.first_name = 'Марсель')
    );

-- Получить уникальные названия уроков
select distinct (lesson.name) from lesson;

-- получить названия уроков и их количество в общем списке уроков
select l.name as lesson_name, count(*) as count from lesson l group by l.name;

-- Получить все курсы и уроки
select * from course c left join lesson l on c.id = l.course_id;
-- Получить все уроки и курсы
select * from course c right join lesson l on c.id = l.course_id;
-- Пересечение
select * from course c inner join lesson l on c.id = l.course_id;
-- Все
select * from course c full outer join lesson l on c.id = l.course_id;

select student st from student where id = 1;