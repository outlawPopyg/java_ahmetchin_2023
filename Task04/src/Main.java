import jdbc.SimpleDataSource;
import models.Student;
import repositories.StudentRepository;
import repositories.StudentRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(properties.getProperty("db.url"), properties.getProperty("db.username"), properties.getProperty("db.password"));
        StudentRepository studentRepository = new StudentRepositoryJdbcImpl(dataSource);

        studentRepository.save(new Student(1L, "Калим", "Ахметшин", 19));
        studentRepository.save(new Student("Борис", "Моисеев", 54));

        System.out.println(studentRepository.findAll());

        System.out.println(studentRepository.findById(1L));
        System.out.println(studentRepository.findById(12L));
    }
}
