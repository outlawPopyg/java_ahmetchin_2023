package repositories;

import models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public class StudentRepositoryJdbcImpl implements StudentRepository {
    //language=PostgreSQL
    private static final String SQL_SELECT_ALL_USERS = "select * from student;";

    //language=SQL
    private static final Function<Student, String> sqlUpdateStudent = s -> {
        String firstName = s.getFirstName();
        String lastName = s.getLastName();
        Integer age = s.getAge();
        if (s.getId() != null) {
            return "update student set " +
                    "first_name = '" + firstName + "'," +
                    " last_name = '" + lastName + "'," +
                    " age = " + age +
                    " where id = " + s.getId();
        } else {
            return "insert into student(first_name, last_name, age) " +
                    "values " + "('" + firstName + "', '" + lastName + "', " + age + ");";
        }
    };


    private static final Function<ResultSet, Student> resultSetMapper = row -> {
        try {
            Long id = row.getLong("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            Integer age = row.getObject("age", Integer.class);
            return new Student(id, firstName, lastName, age);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    private final DataSource dataSource;

    public StudentRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Student> findAll() {

        List<Student> students = new LinkedList<>();

        try (Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_USERS)) {
                while (resultSet.next()) {
                    students.add(resultSetMapper.apply(resultSet));
                }
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return students;
    }

    @Override
    public void save(Student student) {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {

            statement.executeUpdate(sqlUpdateStudent.apply(student));

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Optional<Student> findById(Long id) {
        List<Student> students = findAll();
        return students.stream().filter(s -> Objects.equals(s.getId(), id)).findFirst();
    }
}
