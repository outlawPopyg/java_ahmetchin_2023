package repositories;

import models.Student;

import java.util.List;
import java.util.Optional;

public interface StudentRepository {
    List<Student> findAll();

    void save(Student student);

    Optional<Student> findById(Long id);

}
