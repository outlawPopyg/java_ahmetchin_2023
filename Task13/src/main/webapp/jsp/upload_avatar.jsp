<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
</head>
<body>
	<% if (request.getAttribute("avatarStorageName") == null) { %>
        <b>У вас нет аватара</b>
	<% } else { %>
		<img src="/app/files?fileName=${avatarStorageName}" alt="avatar" />
	<% } %>

	<form action="" method="post" enctype="multipart/form-data">
		<input type="file" name="avatar">
		<button>submit</button>
	</form>
</body>
</html>
