<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
</head>
<body>
	<%
		List<String> images = (List<String>) request.getAttribute("images");
        for (String image : images) { %>
			<li><a href="/app/files?fileName=<%= image %>"> <%= image %></a></li>
	<% } %>
	
</body>
</html>
