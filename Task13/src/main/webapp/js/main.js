document.addEventListener("DOMContentLoaded", function () {
    getAllProducts("", resolveOrderBy()).catch(console.log);
})

function resolveOrderBy() {
    return document.cookie
        .split('; ')
        .find((row) => row.startsWith("sortBy="))?.split('=')[1];
}


const getAllProducts = (query, orderBy = "id") => fetch(`/app/products/search?query=${query}&orderBy=${orderBy}`)
    .then(response => response.json())
    .then(response => insertIntoUl(response));


const insertIntoUl = (data) => {
    let html = "";
    data.forEach(product => html += `<li>id: ${product.id}, title: ${product.title}</li>`)
    document.querySelector("ul#products").innerHTML = html;
}

const addProduct = () => {
    const body = {
        title: document.querySelector("input#title").value,
        count: document.querySelector("input#count").value,
        color: document.querySelector("input#color").value,
        weight: document.querySelector("input#weight").value
    };

    fetch(`/app/products?orderBy=${resolveOrderBy()}`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(res => res.json()).then(insertIntoUl).catch(console.log);
}