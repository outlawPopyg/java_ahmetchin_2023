package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.SignUpDto;
import ru.itis.dto.UserDto;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;
import ru.itis.services.UsersService;

import java.util.List;
import java.util.Optional;

import static ru.itis.dto.UserDto.from;

@RequiredArgsConstructor
@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    @Override
    public void signUp(SignUpDto signUpData) {
            User user = User.builder()
                    .firstName(signUpData.getFirstName())
                    .lastName(signUpData.getLastName())
                    .email(signUpData.getEmail())
                    .password(signUpData.getPassword())
                    .build();

            usersRepository.save(user);
    }

    @Override
    public List<UserDto> getAllUsers() {
        return from(usersRepository.findAll());
    }

    @Override
    public List<User> getAllUsersByAge(int ageFrom, int ageTo) {
        return usersRepository.findAllByAgeInRangeOrderByIdDesc(ageFrom, ageTo);
    }

    @Override
    public void updateUserAvatar(User user) {
        usersRepository.update(user);
    }

    @Override
    public User findUserById(Long id) {
        Optional<User> user = usersRepository.findById(id);
        if (user.isPresent()) {
            return user.get();
        }

        throw new IllegalArgumentException("No such user");
    }
}
