package ru.itis.services;

import java.util.List;

public interface ImageService {
    List<String> findAllStorageNamesOfImages();
    String findStorageNameById(Long id);
}
