package ru.itis.services;

import ru.itis.models.Product;

import java.util.List;

public interface ProductService {
    boolean addProduct(Product product);
    List<Product> getAllProducts();
    boolean deleteProduct(Long id);
    List<Product> getAllProductsOrderBy(String orderBy);

}
