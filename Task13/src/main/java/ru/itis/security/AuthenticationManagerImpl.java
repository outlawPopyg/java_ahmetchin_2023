package ru.itis.security;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;

import java.util.Optional;

@RequiredArgsConstructor
@Component
public class AuthenticationManagerImpl implements AuthenticationManager {

    private final UsersRepository usersRepository;

    @Override
    public Optional<User> authenticate(String email, String password) {
        User user = usersRepository.findOneByEmail(email).orElse(null);

        if (user == null) {
            return Optional.empty();
        }

        if (user.getPassword().equals(password)) {
            return Optional.of(user);
        }

        return Optional.empty();
    }
}
