package ru.itis.security;

import ru.itis.models.User;

import java.util.Optional;

public interface AuthenticationManager {
    Optional<User> authenticate(String email, String password);
}
