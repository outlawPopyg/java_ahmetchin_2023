package ru.itis.security.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.Arrays;

import static ru.itis.constants.Paths.APPLICATION_PREFIX;
import static ru.itis.constants.Paths.SIGN_IN_PAGE;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_IN_PAGE)) {
            filterChain.doFilter(request, response);
            return;
        }

        HttpSession session = request.getSession(false);

        if (session != null) {
            Boolean authenticated = (Boolean) session.getAttribute("authenticated");
            if (authenticated != null && authenticated) {
                filterChain.doFilter(request, response);
            }
        } else {
            Cookie redirectAfterAuth = new Cookie("redirectAfterAuth", request.getRequestURI());
            redirectAfterAuth.setPath("/");
            response.addCookie(redirectAfterAuth);
            response.sendRedirect(APPLICATION_PREFIX + SIGN_IN_PAGE);
        }

    }


}
