package ru.itis.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.itis.models.FileInfo;

import javax.sql.DataSource;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class FilesRepositoryImpl implements FilesRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public FilesRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    // language=SQL
    private final static String SQL_SELECT_BY_STORAGE_NAME =
            "select * from files_info where storage_file_name = :storageFileName";

    //language=SQL
    private final static String SQL_SELECT_BY_TYPES = "select * from files_info where mime_type in (:types)";

    // language=SQL
    private final static String SQL_SELECT_BY_ID =
            "select * from files_info where id = :id";


    private final static RowMapper<FileInfo> fileInfoRowMapper = (row, rowNumber) -> FileInfo.builder()
            .id(row.getLong("id"))
            .originalName(row.getString("original_file_name"))
            .storageName(row.getString("storage_file_name"))
            .size(row.getLong("size"))
            .mimeType(row.getString("mime_type"))
            .description(row.getString("description"))
            .build();

    @Override
    public void save(FileInfo info) {
        Map<String, Object> params = new HashMap<>();
        params.put("originalFileName", info.getOriginalName());
        params.put("storageFileName", info.getStorageName());
        params.put("size", info.getSize());
        params.put("mimeType", info.getMimeType());
        params.put("description", info.getDescription());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("files_info")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(params)).longValue();

        info.setId(id);
    }

    @Override
    public Optional<FileInfo> findByStorageFileName(String fileName) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_STORAGE_NAME,
                    Collections.singletonMap("storageFileName", fileName), fileInfoRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<String> findAllStorageNamesByType(String... types) {
        List<String> typesAsArray = Arrays.stream(types).toList();
        List<FileInfo> files = namedParameterJdbcTemplate.query(SQL_SELECT_BY_TYPES,
                Collections.singletonMap("types", typesAsArray), fileInfoRowMapper);
        return files.stream().map(FileInfo::getStorageName).collect(Collectors.toList());
    }

    @Override
    public Optional<FileInfo> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id), fileInfoRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }


}
