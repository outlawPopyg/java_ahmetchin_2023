package ru.itis.servlets;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDTO;
import ru.itis.models.User;
import ru.itis.services.FilesService;
import ru.itis.services.ImageService;
import ru.itis.services.UsersService;

import java.io.IOException;

@WebServlet(name = "UploadAvatarServlet", value = "/avatar")
@MultipartConfig
public class UploadAvatarServlet extends HttpServlet {
    private ImageService imagesService;
    private FilesService filesService;
    private UsersService usersService;

    @Override
    public void init(ServletConfig config)  {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.imagesService = context.getBean(ImageService.class);
        this.filesService = context.getBean(FilesService.class);
        this.usersService = context.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        User authUser = (User) session.getAttribute("authUser");

        User user = usersService.findUserById(authUser.getId());

        Long fileInfoId = user.getFileInfoId();
        System.out.println(authUser.getId() + " " + fileInfoId);

        if (fileInfoId != null) {
            request.setAttribute("avatarStorageName", imagesService.findStorageNameById(fileInfoId));
        }

        request.getRequestDispatcher("/jsp/upload_avatar.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part file = request.getPart("avatar");
        HttpSession session = request.getSession(false);

        FileDTO uploadedFile = FileDTO.builder()
                .size(file.getSize())
                .mimeType(file.getContentType())
                .fileInputStream(file.getInputStream())
                .fileName(file.getSubmittedFileName())
                .description("avatar")
                .build();

        User authUser = (User) session.getAttribute("authUser");
        User user = usersService.findUserById(authUser.getId());

        Long fileId = filesService.upload(uploadedFile);
        user.setFileInfoId(fileId);

        usersService.updateUserAvatar(user);

        response.sendRedirect("/app/avatar");
    }
}
