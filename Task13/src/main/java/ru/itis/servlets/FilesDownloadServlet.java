package ru.itis.servlets;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDTO;
import ru.itis.services.FilesService;

import java.io.IOException;
import java.nio.file.Files;

@WebServlet(name = "FilesDownloadServlet", value = "/files")
@MultipartConfig
public class FilesDownloadServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FilesService.class);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String storageFileName = request.getParameter("fileName");
        FileDTO file = filesService.getFile(storageFileName);

        response.setContentType(file.getMimeType());
        response.setContentLength(file.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + file.getOriginalFileName() + "\"");
        Files.copy(file.getPath(), response.getOutputStream());
        response.flushBuffer();

    }

}
