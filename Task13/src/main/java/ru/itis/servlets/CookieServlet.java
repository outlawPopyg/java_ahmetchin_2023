package ru.itis.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ru.itis.constants.Paths;

import java.io.IOException;

@WebServlet(name = "CookieServlet", urlPatterns = Paths.SORT_PRODUCTS_LIST)
public class CookieServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sortBy = request.getParameter("sortBy");
        Cookie sortByCookie = new Cookie("sortBy", sortBy);
        sortByCookie.setPath("/");
        response.addCookie(sortByCookie);
        response.sendRedirect("/app/html/addProduct.html");
    }
}
