package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.SignUpDto;
import ru.itis.dto.UserDto;
import ru.itis.dto.converters.http.HttpFormsConverter;
import ru.itis.services.UsersService;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

import static ru.itis.constants.Paths.*;

@WebServlet(name = "usersServlet", urlPatterns = {USERS_PATH, SIGN_UP_PATH}, loadOnStartup = 1)
public class UsersServlet extends HttpServlet {
    private UsersService usersService;

    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.usersService = context.getBean(UsersService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_PATH)) {
            SignUpDto signUpData = HttpFormsConverter.from(request);
            usersService.signUp(signUpData);
            response.sendRedirect(APPLICATION_PREFIX + SEARCH_PAGE);
        } else if (request.getRequestURI().equals(APPLICATION_PREFIX + USERS_PATH)) {
            String body = request.getReader().readLine();
            SignUpDto userData = objectMapper.readValue(body, SignUpDto.class);
            usersService.signUp(userData);
            List<UserDto> users = usersService.getAllUsers();
            String jsonResponse = objectMapper.writeValueAsString(users);
            response.setContentType("application/json");
            response.getWriter().write(jsonResponse);
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
