package ru.itis.servlets;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.springframework.context.ApplicationContext;
import ru.itis.services.ImageService;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "ImagesServlet", value = "/images")
public class ImagesServlet extends HttpServlet {

    private ImageService imagesService;

    @Override
    public void init(ServletConfig config)  {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.imagesService = context.getBean(ImageService.class);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> images = imagesService.findAllStorageNamesOfImages();
        request.setAttribute("images", images);

        request.getRequestDispatcher("/jsp/images.jsp").forward(request, response);
    }

}
