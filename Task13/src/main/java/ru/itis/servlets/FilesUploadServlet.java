package ru.itis.servlets;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDTO;
import ru.itis.services.FilesService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@WebServlet(name = "FilesServlet", urlPatterns = "/files/upload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FilesService.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String description = new BufferedReader(
                new InputStreamReader(request.getPart("description").getInputStream())
        ).readLine();

        Part file = request.getPart("file");

        FileDTO uploadedFile = FileDTO.builder()
                .size(file.getSize())
                .mimeType(file.getContentType())
                .fileInputStream(file.getInputStream())
                .fileName(file.getSubmittedFileName())
                .description(description)
                .build();

        filesService.upload(uploadedFile);
    }
}
