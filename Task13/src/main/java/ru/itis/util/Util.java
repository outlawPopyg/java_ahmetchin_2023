package ru.itis.util;


import ru.itis.models.Product;

import java.util.List;

public class Util {
    public static String getHtmlForUsers(List<Product> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\t<title>Users</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>Title</th>\n" +
                "\t\t<th>Count</th>\n" +
                "\t\t<th>Color</th>\n" +
                "\t\t<th>Weight</th>\n" +
                "\t</tr>");

        for (Product product : products) {
            html.append("<tr>\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getTitle()).append("</td>\n");
            html.append("<td>").append(product.getCount()).append("</td>\n");
            html.append("<td>").append(product.getColor()).append("</td>\n");
            html.append("<td>").append(product.getWeight()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("<form action=\"/app/products/find\" method=\"get\">\n" +
                "\t\t<label>\n" +
                "\t\t\tSearch: <input name=\"query\" type=\"text\">\n" +
                "\t\t</label>\n" +
                "\t\t<button>find</button>\n" +
                "\t</form>");

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");

        return html.toString();
    }
}
