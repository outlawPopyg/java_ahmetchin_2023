drop table if exists student;

create table student
(
    id         bigserial primary key, -- идентификатор строки - всегда уникальный
    first_name varchar(20),
    last_name  varchar(20),
    age        integer check ( age > 18 and age < 120),
    email varchar(30),
    password varchar(30)
);