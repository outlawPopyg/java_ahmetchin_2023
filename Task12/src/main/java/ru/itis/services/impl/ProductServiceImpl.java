package ru.itis.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.ProductService;

import java.util.List;

@Repository
public class ProductServiceImpl implements ProductService {

    private final ProductsRepository productsRepository;

    @Autowired
    public ProductServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public boolean addProduct(Product product) {
        try {
            productsRepository.save(product);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public boolean deleteProduct(Long id) {

        if (productsRepository.findById(id).isEmpty()) {
            System.err.println("Такого товара не существует");
            return false;
        }

        try {
            productsRepository.delete(id);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public List<Product> getAllProductsOrderBy(String orderBy) {
        return productsRepository.findAllProductsOrderBy(orderBy);
    }
}
