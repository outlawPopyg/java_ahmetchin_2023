package ru.itis.services;

import ru.itis.dto.ProductDTO;

import java.util.List;

public interface SearchService {
    public List<ProductDTO> searchProducts(String query, String orderBy);
}
