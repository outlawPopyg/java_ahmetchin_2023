package ru.itis.constants;

public class Paths {
    public static final String APPLICATION_PREFIX = "/app";

    public static final String SIGN_UP_PATH = "/signUp";
    public static final String SIGN_IN_PATH = "/signIn";
    public static final String USERS_PATH = "/users";

    public static final String COLOR_CHANGE_PATH = "/profile/color";

    public static final String SEARCH_PAGE = "/users/search.html";

    public static final String PROFILE_PAGE = "/profile.html";

    public static final String SIGN_IN_PAGE = "/signIn.html";

    public static final String USERS_SEARCH_PATH = "/users/search";

    public static final String LOG_OUT_PATH  = "/logout";
    public static final String LOG_OUT_PAGE  = "/logout.html";
    public static final String PRODUCTS_PATH = "/products";
    public static final String FIND_PRODUCTS_PATH = "/products/find";
    public static final String SORT_PRODUCTS_LIST = "/products/sort";
}
