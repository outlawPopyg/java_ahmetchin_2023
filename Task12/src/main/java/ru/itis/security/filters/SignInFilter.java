package ru.itis.security.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.context.ApplicationContext;
import ru.itis.security.AuthenticationManager;

import java.io.IOException;

import static ru.itis.constants.Paths.*;

@WebFilter(SIGN_IN_PATH)
public class SignInFilter implements Filter {

    private AuthenticationManager authenticationManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = (ApplicationContext) filterConfig.getServletContext().getAttribute("springContext");
        this.authenticationManager = context.getBean(AuthenticationManager.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getMethod().equals("POST")) {
            String email = request.getParameter("email");
            String password = request.getParameter("password");

            if (authenticationManager.authenticate(email, password)) {
                HttpSession session = request.getSession(true);
                session.setAttribute("authenticated", true);

                response.sendRedirect(getRedirectPath(request));
            } else {
                response.sendRedirect(APPLICATION_PREFIX + SIGN_IN_PAGE + "?error");
            }
        } else if (request.getMethod().equals("GET")) {

            HttpSession session = request.getSession(false);
            if (session != null) {

                response.sendRedirect(APPLICATION_PREFIX + PROFILE_PAGE);
                return;
            }

            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private String getRedirectPath(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("redirectAfterAuth")) {
                return cookie.getValue();
            }
        }

        return APPLICATION_PREFIX + PROFILE_PAGE;
    }
}
