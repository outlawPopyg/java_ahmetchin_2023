package ru.itis.security.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Arrays;

import static ru.itis.constants.Paths.*;

@WebFilter(LOG_OUT_PATH)
public class LogOutFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        if (req.getMethod().equals("POST")) {

            req.getSession(false).invalidate();

            res.sendRedirect(APPLICATION_PREFIX + SIGN_IN_PATH);
            return;
        }

        chain.doFilter(req, res);
    }
}
