package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import ru.itis.models.Product;
import ru.itis.services.ProductService;
import ru.itis.util.Util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static ru.itis.constants.Paths.APPLICATION_PREFIX;
import static ru.itis.constants.Paths.PRODUCTS_PATH;

@WebServlet(urlPatterns = PRODUCTS_PATH)
public class ProductsServlet extends HttpServlet {
    private ProductService service;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.service = context.getBean(ProductService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Product> products = service.getAllProducts();

        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        String html = Util.getHtmlForUsers(products);

        writer.println(html);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body = req.getReader().readLine();
        Product product = objectMapper.readValue(body, Product.class);
        service.addProduct(product);

        String orderBy = req.getParameter("orderBy");
        List<Product> products = service.getAllProductsOrderBy(orderBy);
        String jsonResponse = objectMapper.writeValueAsString(products);

        resp.setContentType("application/json");
        resp.getWriter().write(jsonResponse);
    }


}
