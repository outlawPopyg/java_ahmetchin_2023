package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import ru.itis.dto.ProductDTO;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.ProductService;
import ru.itis.services.SearchService;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "SearchServlet", value = "/products/search")
public class SearchServlet extends HttpServlet {

    private ObjectMapper mapper;
    private SearchService searchService;
    private ProductService productService;
    private ProductsRepository productsRepository;
    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");

        this.mapper = context.getBean(ObjectMapper.class);
        this.searchService = context.getBean(SearchService.class);
        this.productService = context.getBean(ProductService.class);
        this.productsRepository = context.getBean(ProductsRepository.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String query = request.getParameter("query");
        String orderBy = request.getParameter("orderBy");
        String jsonResponse;

        if (query == null || query.equals("") && orderBy != null && !orderBy.equals("")) {
            List<Product> products = productService.getAllProductsOrderBy(orderBy);
            System.out.println(products);
            jsonResponse = mapper.writeValueAsString(products);
        } else {
            List<ProductDTO> productDTOS = searchService.searchProducts(query, orderBy);
            jsonResponse = mapper.writeValueAsString(productDTOS);
        }

        response.setContentType("application/json");
        response.getWriter().print(jsonResponse);
    }

}
