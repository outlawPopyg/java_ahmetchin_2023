select c.model, c.speed, c.hd
from PC c
where c.price < 500
--
select distinct(product.maker)
from Product product
where product.type = 'Printer'
--
select p.model, p.ram, p.screen
from Laptop p
where p.price > 1000
--
select *
from Printer p
where p.color = 'y'
--
select p.model, p.speed, p.hd
from PC p
where (p.cd = '12x' or p.cd = '24x')
  and p.price < 600
--
select distinct Product.maker, Laptop.speed
from Product
         inner join Laptop on Laptop.hd >= 10 and Laptop.model = Product.model
--
select a.model, price
from (select model, price
      from PC
      union
      select model, price
      from Laptop
      union
      select model, price
      from Printer) as a
         inner join Product p on a.model = p.model and p.maker = 'B'
--
select p.maker
from Product p
where p.type = 'PC'
EXCEPT
select p.maker
from Product p
where p.type = 'Laptop'
--
select distinct Product.maker
from Product
         inner join PC on PC.model = Product.model and Product.type = 'PC' and PC.speed >= 450
--
select model, price
from Printer
where price in (select max(price) from Printer)
--
select avg(speed)
from PC
--
select avg(speed)
from Laptop l
where l.price > 1000
--
select avg(speed)
from PC pc
         join Product pr on pr.model = pc.model and pr.maker = 'A'
--
select Ships.class, Ships.name, Classes.country
from Ships
         join Classes on Classes.class = Ships.class and Classes.numGuns >= 10
--
select hd
from PC
group by hd
having count(hd) >= 2
--
select distinct i.model, j.model, i.speed, i.ram
from PC i,
     PC j
where i.speed = j.speed
  and i.ram = j.ram
  and i.model != j.model and i.model > j.model
--
select distinct Product.type, Laptop.model, Laptop.speed
from Laptop
         join Product on Product.model = Laptop.model and Laptop.speed < all (select speed from PC)
--
select distinct Product.maker, price
from Printer
         join Product on Printer.model = Product.model and Printer.color = 'y' and
                         price in (select min(price) from Printer p where p.color = 'y')
--
select Product.maker, avg(screen)
from Laptop
         join Product on Laptop.model = Product.model
group by Product.maker
--
select distinct maker, count(model)
from Product
where type = 'PC'
group by maker
having count(model) >= 3
--
select Product.maker, max(price)
from PC
         join Product on Product.model = PC.model
group by Product.maker
--
select speed, avg(price)
from PC
where speed > 600
group by speed
--
select speed, avg(price)
from PC
where speed > 600
group by speed
--
select Product.maker
from Product
         join PC on PC.speed >= 750 and Product.model = PC.model
intersect
select Product.maker
from Product
         join Laptop on Laptop.speed >= 750 and Product.model = Laptop.model
--
select model
from (select model, price
      from PC
      union
      select model, price
      from Laptop
      union
      select model, price
      from Printer) as models
where models.price in (select max(price)
                       from (select price
                             from PC
                             union
                             select price
                             from Laptop
                             union
                             select price
                             from Printer) as prices)
--
select distinct Product.maker
from Product
where Product.type = 'Printer'
  and Product.maker in
      (select distinct maker
       from Product
                join PC on Product.model = PC.model and ram = (select min(ram) from PC) and
                           speed = (select max(speed) from PC where ram = (select min(ram) from PC)))
--
    with PC_and_Laptop as (
select pc.price, pt.maker
from Product pt
join PC pc on pt.model = pc.model
where pt.maker = 'A'
union all
select l.price, pt.maker
from Product pt
join Laptop l on pt.model = l.model
where pt.maker = 'A'
)

select avg(price)
from PC_and_Laptop

--
select pt.maker, avg(pc.hd)
from Product pt
         right join PC pc on pt.model = pc.model
where pt.maker in (select distinct maker from Product where type = 'Printer')
group by pt.maker

--
select count(maker)
from Product
where maker in (select maker from Product group by maker having count(model) = 1)

--
select oo.point, oo.date, io.inc, oo.out
from Outcome_o oo
         left join Income_o io on oo.point = io.point and oo.date = io.date
union
select io.point, io.date, io.inc, oo.out
from Outcome_o oo
         right join Income_o io on oo.point = io.point and oo.date = io.date

--
select class, country
from Classes
where bore >= 16

--
select ship
from Outcomes
where result = 'sunk'
  and battle = 'North Atlantic'

--
select distinct sh.name
from Classes cl
         full join Ships sh on cl.class = sh.class
where cl.displacement > 35000
  and sh.launched >= 1922
  and cl.type = 'bb'
  and sh.launched is not Null

--
select model, type
from Product
where (model like '%[^A-Z]%' and model not like '%[^0-9]%')
   or (model not like '%[^A-Z]%' and model like '%[^0-9]%')

--
select cl.class
from Classes cl
         join Ships sh on sh.name = cl.class
union
select cl.class
from Classes cl
         join Outcomes ou on ou.ship = cl.class

--
select country
from Classes
where type = 'bb'
INTERSECT
select country
from Classes
where type = 'bc'

--
    with ship_battle_damage as (
Select ou.ship as ship, bt.date as battle_date
from Outcomes ou
join Battles bt on ou.battle = bt.name
where ou.result = 'damaged')

select distinct ship
from ship_battle_damage
where battle_date < any (select battle_dateA
                         from (Select ou.ship as shipAL, bt.date as battle_dateA
                               from Outcomes ou
                                        join Battles bt on ou.battle = bt.name) r
                         where shipAL = ship)

--
select distinct maker, type
from product
where maker in (select maker
                from (select distinct maker, type
                      from Product
                      where maker in (Select maker
                                      from Product
                                      group by maker
                                      having count(model) > 1)) t
                group by maker
                having count(type) = 1)






















