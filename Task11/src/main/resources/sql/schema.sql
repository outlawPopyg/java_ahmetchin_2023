create table if not exists product
(
    id bigserial primary key,
    title varchar(20),
    count integer,
    color varchar(20),
    weight integer
);

insert into product (title, count, color, weight) VALUES
        ('Lada Granta', 12, 'Metallic', 1299),
        ('Lada NIVA', 143, 'Metallic', 1000);


