const getAllProducts = (query) => fetch(`/app/products/search?query=${query}`)
    .then(response => response.json())
    .then(response => insertIntoUl(response));


const insertIntoUl = (data) => {

    let html = "";
    data.forEach(product => html += `<li>id: ${product.id}, title: ${product.title}</li>`)
    document.getElementById("ul").innerHTML = html;
}