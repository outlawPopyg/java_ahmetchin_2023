package org.example.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.example.dto.ProductDTO;
import org.example.services.SearchService;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "SearchServlet", value = "/products/search")
public class SearchServlet extends HttpServlet {

    private ObjectMapper mapper;
    private SearchService searchService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");

        this.mapper = context.getBean(ObjectMapper.class);
        this.searchService = context.getBean(SearchService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = request.getParameter("query");
        List<ProductDTO> productDTOS = searchService.searchProducts(query);
        String jsonResponse = mapper.writeValueAsString(productDTOS);
        response.setContentType("application/json");
        response.getWriter().print(jsonResponse);
    }

}
