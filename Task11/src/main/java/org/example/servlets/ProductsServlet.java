package org.example.servlets;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.config.ApplicationConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import org.example.models.Product;
import org.example.util.Util;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.example.services.ProductService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import static org.example.constants.Paths.*;

@WebServlet(urlPatterns = PRODUCTS_PATH)
public class ProductsServlet extends HttpServlet {
    private ProductService service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.service = context.getBean(ProductService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Product> products = service.getAllProducts();

        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        String html = Util.getHtmlForUsers(products);

        writer.println(html);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        Integer count = Integer.valueOf(req.getParameter("count"));
        String color = req.getParameter("color");
        Integer weight = Integer.parseInt(req.getParameter("weight"));

        Product product = Product.builder()
                .title(title).count(count).color(color).weight(weight)
                .build();

        service.addProduct(product);
        resp.sendRedirect(APPLICATION_PREFIX + PRODUCTS_PATH);
    }


}
