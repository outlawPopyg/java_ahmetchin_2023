package org.example.servlets;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.config.ApplicationConfig;
import org.example.models.Product;
import org.example.services.ProductService;
import org.example.util.Util;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;
import static org.example.constants.Paths.*;

@WebServlet(urlPatterns = FIND_PRODUCTS_PATH)
public class FindProduct extends HttpServlet {

    ProductService service;
    HikariDataSource dataSource;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.service = context.getBean(ProductService.class);
        this.dataSource = context.getBean(HikariDataSource.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String query = req.getParameter("query");
        List<Product> sortedList = service.getAllProducts().stream()
                .filter(product -> product.getTitle().contains(query))
                .collect(Collectors.toList());

        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        String html = Util.getHtmlForUsers(sortedList);

        writer.println(html);
    }

    @Override
    public void destroy() {
        this.dataSource.close();
    }
}
