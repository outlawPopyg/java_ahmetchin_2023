package org.example.services;

import org.example.dto.ProductDTO;

import java.util.List;

public interface SearchService {
    public List<ProductDTO> searchProducts(String query);
}
