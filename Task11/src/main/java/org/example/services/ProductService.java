package org.example.services;

import org.example.models.Product;

import java.util.List;

public interface ProductService {
    boolean addProduct(Product product);
    List<Product> getAllProducts();
    boolean deleteProduct(Long id);

}
