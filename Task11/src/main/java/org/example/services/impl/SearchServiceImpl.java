package org.example.services.impl;

import org.example.dto.ProductDTO;
import org.example.repositories.ProductsRepository;
import org.example.services.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.example.dto.ProductDTO.from;

import java.util.Collections;
import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    private final ProductsRepository productsRepository;

    @Autowired
    public SearchServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public List<ProductDTO> searchProducts(String query) {
        if (query == null || query.equals("")) {
            return Collections.emptyList();
        }
        return from(productsRepository.findAllProductsByTitle(query));
    }
}
