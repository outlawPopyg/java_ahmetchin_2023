package org.example.constants;

public class Paths {
    public static final String APPLICATION_PREFIX = "/app";
    public static final String PRODUCTS_PATH = "/products";
    public static final String FIND_PRODUCTS_PATH = "/products/find";
}
