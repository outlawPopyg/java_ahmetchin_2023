import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import models.Product;
import repositories.ProductsRepository;
import repositories.ProductsRepositoryJdbcTemplateImpl;
import java.io.IOException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setPassword(properties.getProperty("db.password"));
        config.setUsername(properties.getProperty("db.username"));
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);
        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);

        productsRepository.save(Product.builder()
                        .title("Toyota Camry")
                        .count(233)
                        .color("black")
                        .weight(1500)
                .build());

        productsRepository.save(Product.builder()
                .title("Лада Ларгус")
                .count(1)
                .color("metallic")
                .weight(2100)
                .build());

        System.out.println(productsRepository.findById(1L));

        Product product = Product.builder()
                .id(1L).count(232).weight(1500).color("black").build();
        productsRepository.update(product);

        productsRepository.delete(1L);

        System.out.println(productsRepository.findAllByWeightGreaterThanOrderByIdDesc(11));

    }
}
