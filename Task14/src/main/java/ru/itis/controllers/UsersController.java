package ru.itis.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.SignUpDto;
import ru.itis.dto.UserDto;
import ru.itis.services.UsersService;

import java.util.List;

/**
@RequiredArgsConstructor
@Controller
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping("/users")
public class UsersController {

    UsersService usersService;

    @RequestMapping
    public String getUsersPage(Model model) {
        model.addAttribute("users", usersService.getAllUsers());
        return "users";
    }

    @RequestMapping(value = "/search")
    @ResponseBody
    public List<UserDto> searchUsers(@RequestParam("query") String query) {
        return usersService.searchUsers(query);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public List<UserDto> addUser(@RequestBody SignUpDto signUpData) {
        return usersService.signUp(signUpData);
    }
}
**/