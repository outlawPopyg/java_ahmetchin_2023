package ru.itis.controllers;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.itis.dto.ProductDTO;
import ru.itis.services.ProductService;
import ru.itis.services.SearchService;
import ru.itis.services.impl.SearchServiceImpl;

import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor
@Controller
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping("/products")
public class ProductsSearchController {
    SearchService searchService;
    ProductService productService;

    @RequestMapping
    public String getSearchPage(Model model) {
        return "liveSearch";
    }

    @RequestMapping("/search")
    @ResponseBody
    public List<ProductDTO> getProducts(@RequestParam("query") String query, @RequestParam("orderBy") String orderBy) {
        if (query == null || query.equals("") && orderBy != null && !orderBy.equals("")) {
            return ProductDTO.from(productService.getAllProductsOrderBy(orderBy));
        } else {
            return searchService.searchProducts(query, orderBy);
        }
    }
}
