package ru.itis.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.models.Product;
import ru.itis.services.ProductService;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/products")
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class FindProductsController {
    ProductService productService;

    @RequestMapping("/find")
    public String getProductFindPage() {
        return "find";
    }

    @RequestMapping("/findProducts")
    public String getProductFindPage(
            @RequestParam("title") String title,
            @RequestParam("count") Integer count,
            @RequestParam("color") String color,
            @RequestParam("weight") Integer weight,
            Model model) {

        List<Product> list = productService.getAllProducts().stream().filter(product -> {
            return product.getTitle().equals(title) || product.getWeight().equals(weight) || product.getColor().equals(color)
                    || product.getCount().equals(count);
        }).toList();

        model.addAttribute("list", list);
        return "products";
    }

}
