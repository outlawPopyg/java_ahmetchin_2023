package ru.itis.controllers;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.models.Product;
import ru.itis.services.ProductService;

import java.io.IOException;

@RequestMapping("/products")
@Controller
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ProductController {
    ProductService productService;

    @RequestMapping("/add")
    public String getAddProductPage() {
        return "addProduct";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addProduct(@RequestParam("title") String title, HttpServletResponse response) throws IOException {
        productService.addProduct(Product.builder().title(title).build());
        response.sendRedirect("/products/add");
    }
}
