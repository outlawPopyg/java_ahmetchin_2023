package ru.itis.controllers;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;

@Controller
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequestMapping("/profile")
@RequiredArgsConstructor
public class ProfileController {

    @RequestMapping
    public String getProfilePage() {
        return "profile";
    }

    @RequestMapping("/readCookie")
    public void readCookie(@CookieValue("sortBy") Cookie cookie) {
        System.out.println(cookie.getName());
    }

    @RequestMapping(method = RequestMethod.POST)
    public void setCookie(@RequestParam("sortBy") String sortBy, HttpServletResponse response) throws IOException {
        Cookie cookie = new Cookie("sortBy", sortBy);
        cookie.setMaxAge(3600);
        response.addCookie(cookie);
        response.sendRedirect("/profile");
    }
}
