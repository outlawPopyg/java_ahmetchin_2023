package ru.itis.repositories;

import ru.itis.models.FileInfo;

import java.util.List;
import java.util.Optional;

public interface FilesRepository {
    void save(FileInfo info);
    Optional<FileInfo> findByStorageFileName(String fileName);
    List<String> findAllStorageNamesByType(String ... types);
    Optional<FileInfo> findById(Long id);
}
