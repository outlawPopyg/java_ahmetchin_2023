package ru.itis.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import ru.itis.models.Product;

import javax.sql.DataSource;
import java.util.*;

@Component
public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {
    // language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from product";
    // language=SQL
    private static final String SQL_GET_PRODUCT_BY_ID = "select * from product where id = :id";

    // language=SQL
    private static final String SQL_UPDATE_PRODUCT_BY_ID = "update product set title = :title, count = :count, " +
            "weight = :weight, color = :color where id = :id";

    // language=SQL
    private static final String SQL_DELETE_PRODUCT_BY_ID = "delete from product where id = :id";

    // language=SQL
    private static final String SQL_FIND_ALL_BY_WEIGHT_GREATER_THAN_ORDER_BY_ID_DESC =
            "select * from product where weight > :weight order by id desc";

    // language=SQL
    private static final String SQL_FIND_ALL_LIKE_TITLE =
            "select * from product where title ilike :query order by ";
    // language=SQL
    private static final String SQL_FIND_ALL_ORDER_BY =
            "select * from product order by ";



    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productMapper = ((row, rowNum) -> Product.builder()
            .id(row.getLong("id"))
            .title(row.getString("title"))
            .count(row.getObject("count", Integer.class))
            .color(row.getString("color"))
            .weight(row.getObject("weight", Integer.class))
            .build());


    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productMapper);
    }

    @Override
    public void save(Product product) {
        Map<String, Object> paramsMap = new HashMap<>();

        paramsMap.put("title", product.getTitle());
        paramsMap.put("color", product.getColor());
        paramsMap.put("count", product.getCount());
        paramsMap.put("weight", product.getWeight());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("product")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsMap)).longValue();

        product.setId(id);
    }

    @Override
    public Optional<Product> findById(Long id) {
        try {
            return Optional.ofNullable(
                    namedParameterJdbcTemplate.queryForObject(
                            SQL_GET_PRODUCT_BY_ID,
                            Collections.singletonMap("id", id),
                            productMapper
                    )
            );
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Product product) {
        Map<String, Object> paramsMap = new HashMap<>();

        paramsMap.put("id", product.getId());
        paramsMap.put("title", product.getTitle());
        paramsMap.put("count", product.getCount());
        paramsMap.put("color", product.getColor());
        paramsMap.put("weight", product.getWeight());

        namedParameterJdbcTemplate.update(SQL_UPDATE_PRODUCT_BY_ID, paramsMap);
    }

    @Override
    public void delete(Long id) {
        namedParameterJdbcTemplate.update(SQL_DELETE_PRODUCT_BY_ID, Collections.singletonMap("id", id));
    }

    @Override
    public List<Product> findAllByWeightGreaterThanOrderByIdDesc(int minWeight) {
        return namedParameterJdbcTemplate.query(
                SQL_FIND_ALL_BY_WEIGHT_GREATER_THAN_ORDER_BY_ID_DESC,
                Collections.singletonMap("weight", minWeight),
                productMapper
        );
    }

    @Override
    public List<Product> findAllProductsByTitle(String query, String orderBy) {
        Map<String,String> map = new HashMap<>();
        map.put("query", "%" + query + "%");
        return namedParameterJdbcTemplate.query(
                getSQLWithOrderBySuffix(SQL_FIND_ALL_LIKE_TITLE, orderBy),
                map,
                productMapper
        );
    }






    @Override
    public List<Product> findAllProductsOrderBy(String orderBy) {
        return namedParameterJdbcTemplate.query(getSQLWithOrderBySuffix(SQL_FIND_ALL_ORDER_BY, orderBy), productMapper);
    }

    private String getSQLWithOrderBySuffix(String SQLPrefix, String name) {
        switch (name) {
            case "title":
                return SQLPrefix + "title";
            case "count":
                return SQLPrefix + "count";
            case "color":
                return SQLPrefix + "color";
            case "weight":
                return SQLPrefix + "weight";
            default:
                return SQLPrefix + "id";
        }
    }
}
