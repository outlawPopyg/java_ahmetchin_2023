package ru.itis.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.itis.models.User;

import javax.sql.DataSource;
import java.util.*;

/**
 * 08.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Repository
public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_STUDENTS = "select * from student;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = :id";

    //language=SQL
    private static final String SQL_LIKE_BY_FIRST_NAME_OR_LAST_NAME = "select * from student where " +
            "(first_name ilike :query or last_name ilike :query)";


    //language=SQL
    private static final String SQL_SELECT_BY_AGE_IN_RANGE_ORDER_BY_ID_DESC =
            "select * from student where age > :minAge and age < :maxAge order by id desc";

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL = "" +
            "select * from student where email = :email";

    private static final RowMapper<User> studentMapper = (row, rowNumber) -> User.builder()
            .id(row.getLong("id"))
            .firstName(row.getString("first_name"))
            .lastName(row.getString("last_name"))
            .age(row.getObject("age", Integer.class))
            .email(row.getString("email"))
            .password(row.getString("password"))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<User> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_STUDENTS, studentMapper);
    }

    @Override
    public void save(User student) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("first_name", student.getFirstName());
        paramsAsMap.put("last_name", student.getLastName());
        paramsAsMap.put("email", student.getEmail());
        paramsAsMap.put("password", student.getPassword());
        paramsAsMap.put("phone_number", "default");

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("student")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();


        student.setId(id);
    }

    @Override
    public Optional<User> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    studentMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(User student) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<User> findAllByAgeInRangeOrderByIdDesc(int minAge, int maxAge) {
        Map<String, Object> params = new HashMap<>();

        params.put("minAge", minAge);
        params.put("maxAge", maxAge);

        return namedParameterJdbcTemplate.query(SQL_SELECT_BY_AGE_IN_RANGE_ORDER_BY_ID_DESC, params ,studentMapper);
    }

    @Override
    public List<User> findAllByFirstNameOrLastNameLike(String query) {
        return namedParameterJdbcTemplate.query(SQL_LIKE_BY_FIRST_NAME_OR_LAST_NAME,
                Collections.singletonMap("query", "%" + query + "%"),
                studentMapper);
    }

    @Override
    public Optional<User> findOneByEmail(String email) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_EMAIL,
                    Collections.singletonMap("email", email),
                    studentMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
}
