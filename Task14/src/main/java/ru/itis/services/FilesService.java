package ru.itis.services;

import ru.itis.dto.FileDTO;

public interface FilesService {
    Long upload(FileDTO file);
    FileDTO getFile(String fileName);
}
