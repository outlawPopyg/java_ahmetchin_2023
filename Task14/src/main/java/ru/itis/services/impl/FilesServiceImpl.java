package ru.itis.services.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.itis.dto.FileDTO;
import ru.itis.models.FileInfo;
import ru.itis.repositories.FilesRepository;
import ru.itis.services.FilesService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static ru.itis.dto.FileDTO.from;

@Service
public class FilesServiceImpl implements FilesService {

    @Value("${storage.path}")
    private String storagePath;

    private final FilesRepository filesRepository;

    public FilesServiceImpl(FilesRepository filesRepository) {
        this.filesRepository = filesRepository;
    }

    @Override
    public Long upload(FileDTO file) {
        String fileName = file.getFileName();
        String extension = fileName.substring(fileName.lastIndexOf("."));
        String storageName = UUID.randomUUID() + extension;

        FileInfo fileInfo = FileInfo.builder()
                .originalName(fileName)
                .storageName(storageName)
                .description(file.getDescription())
                .mimeType(file.getMimeType())
                .size(file.getSize())
                .build();

        try {
            Files.copy(file.getFileInputStream(), Paths.get(storagePath + storageName));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        filesRepository.save(fileInfo);

        return fileInfo.getId();
    }

    @Override
    public FileDTO getFile(String fileName) {
        FileInfo file = filesRepository.findByStorageFileName(fileName).orElseThrow();
        FileDTO fileDto = from(file);
        fileDto.setPath(Paths.get(storagePath +  fileName));
        return fileDto;
    }

}
