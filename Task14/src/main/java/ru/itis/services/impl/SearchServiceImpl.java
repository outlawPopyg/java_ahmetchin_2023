package ru.itis.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.dto.ProductDTO;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.SearchService;

import java.util.List;

import static ru.itis.dto.ProductDTO.from;

@Service
public class SearchServiceImpl implements SearchService {

    private final ProductsRepository productsRepository;

    @Autowired
    public SearchServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public List<ProductDTO> searchProducts(String query, String orderBy) {
        return from(productsRepository.findAllProductsByTitle(query, orderBy));
    }
}
