package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.models.FileInfo;
import ru.itis.repositories.FilesRepository;
import ru.itis.services.ImageService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {
    private final FilesRepository repository;

    @Override
    public List<String> findAllStorageNamesOfImages() {
        return repository.findAllStorageNamesByType("image/jpeg", "image/png");
    }

    @Override
    public String findStorageNameById(Long id) {
        Optional<FileInfo> optional = repository.findById(id);
        if (optional.isPresent()) {
            return optional.get().getStorageName();
        } else {
            throw new IllegalArgumentException("No such file");
        }
    }

}
