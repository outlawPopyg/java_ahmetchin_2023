package ru.itis.services;

import ru.itis.dto.SignUpDto;
import ru.itis.dto.UserDto;
import ru.itis.models.User;

import java.util.List;

public interface UsersService {
    void signUp(SignUpDto signUpData);
    List<UserDto> getAllUsers();
    List<User> getAllUsersByAge(int ageFrom, int ageTo);
    public void updateUserAvatar(User user);
}
