package ru.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.models.FileInfo;

import java.io.InputStream;
import java.nio.file.Path;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FileDTO {
    private Long size;
    private String description;
    private String mimeType;
    private String fileName;
    private String originalFileName;
    private InputStream fileInputStream;
    private Path path;

    public static FileDTO from(FileInfo fileInfo) {
        return FileDTO.builder()
                .description(fileInfo.getDescription())
                .size(fileInfo.getSize())
                .originalFileName(fileInfo.getOriginalName())
                .mimeType(fileInfo.getMimeType())
                .fileName(fileInfo.getStorageName())
                .build();
    }
}
