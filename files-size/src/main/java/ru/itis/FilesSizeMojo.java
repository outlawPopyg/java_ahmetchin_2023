package ru.itis;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Mojo(name = "list-of-sizes", defaultPhase = LifecyclePhase.COMPILE)
public class FilesSizeMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project; // здесь будет лежать проект, в котором используется нащ плагин

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputFolderFileName; // maven сам сюда подставит папку (по умолчанию target

    @Parameter(defaultValue = "${project.build.sourceDirectory}", required = true, readonly = true)
    private String sourceFolderFileName;

    @Parameter(name = "listOfSizesFileName", required = true) // куда мы хотим чтобы все названия классов попали
    private String listOfSizesFileName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        File outputFolder = new File(outputFolderFileName);
        File listOfSizesFile = new File(outputFolder, listOfSizesFileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(listOfSizesFile))) {
            getLog().info("Output file for list of classes is - " + listOfSizesFileName);

            Files.walk(Paths.get(sourceFolderFileName))
                    .filter(Files::isRegularFile)
                    .forEach(file -> {
                        try {
                            writer.write(file.toString() + " - " + file.toFile().length());
                            writer.newLine();
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                    });

            getLog().info("Finish work");
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }
}
