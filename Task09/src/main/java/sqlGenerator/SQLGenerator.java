package sqlGenerator;

import java.lang.reflect.Field;

public class SQLGenerator<T> {

    private String generateCreateSubRequest(ColumnName columnName, String sourceType) {

        String name = columnName.value();
        boolean identity = columnName.identity();
        int maxLength = columnName.maxLength();
        boolean primary = columnName.primary();
        boolean defaultBoolean = columnName.defaultBoolean();

        switch (sourceType) {
            case "boolean":
                return name + " " + sourceType + " default " + defaultBoolean;
            case "Long":
                return name + " bigint" + (primary ? " primary key" : "") + (identity ? " generated always as identity" : "");
            case "String":
                return name + " varchar(" + maxLength + ")";
            default:
                return "";
        }
    }

    public String createTable(Class<T> entityClass) {
        String tableName = entityClass.getAnnotation(TableName.class).value();
        String SQL_REQUEST = "create table " + tableName + " ( ";

        Field[] fields = entityClass.getDeclaredFields();
        StringBuilder subRequest = new StringBuilder();

        int i;
        for (i = 0; i < fields.length - 1; i++) {
            ColumnName columnName = fields[i].getDeclaredAnnotation(ColumnName.class);
            String fieldType = fields[i].getType().getSimpleName();

            subRequest.append(generateCreateSubRequest(columnName, fieldType)).append(",");
        }
        subRequest.append(
                generateCreateSubRequest(
                        fields[i].getDeclaredAnnotation(ColumnName.class),
                        fields[i].getType().getSimpleName()
                )
        );

        return SQL_REQUEST + subRequest + " )";
    }

    public String insert(Object entity) {
        String tableName = entity.getClass().getDeclaredAnnotation(TableName.class).value();
        Field[] fields = entity.getClass().getDeclaredFields();

        String SQL_REQUEST = "insert into " + tableName;

        StringBuilder attributes = new StringBuilder("(");
        StringBuilder values = new StringBuilder("values (");

        for (Field field : fields) {
            try {
                field.setAccessible(true);
                ColumnName columnName = field.getDeclaredAnnotation(ColumnName.class);

                String fieldType = field.getType().getSimpleName();
                Object entityValue = field.get(entity);

                if (fieldType.equals("String")) {
                    entityValue = "'" + entityValue + "'";
                }

                if (entityValue != null) {
                    attributes.append(columnName.value()).append(",");
                    values.append(entityValue).append(",");
                }
                field.setAccessible(false);
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(e);
            }
        }

        return SQL_REQUEST
                + generateInsertSubRequestWithoutLastComma(attributes) + " "
                + generateInsertSubRequestWithoutLastComma(values);
    }

    private String generateInsertSubRequestWithoutLastComma(StringBuilder builder) {
        return builder.substring(0, builder.length()-1) + ")";
    }
}
