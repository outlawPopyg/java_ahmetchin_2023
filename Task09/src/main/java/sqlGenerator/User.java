package sqlGenerator;

@TableName("account")
public class User {
    @ColumnName(value = "id", primary = true, identity = true)
    private Long id;

    @ColumnName(value = "first_name", maxLength = 25)
    private String firstName;

    @ColumnName(value = "last_name", defaultBoolean = true)
    private String lastName;

    @ColumnName(value = "is_worker", defaultBoolean = true)
    private boolean isWorker;

    @ColumnName(value = "age")
    private Long age;

    public User(Long id, String firstName, String lastName, boolean isWorker, Long age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isWorker = isWorker;
        this.age = age;
    }

    public User(String firstName, String lastName, boolean isWorker, Long age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isWorker = isWorker;
        this.age = age;
    }
    public User(String firstName, String lastName, boolean isWorker) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isWorker = isWorker;
    }

    public User(String firstName, String lastName, Long age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
}
