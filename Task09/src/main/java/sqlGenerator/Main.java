package sqlGenerator;

public class Main {
    public static void main(String[] args) {
        SQLGenerator<User> sqlGenerator = new SQLGenerator<>();
        System.out.println(sqlGenerator.createTable(User.class));

        User user = new User("Kalim", "Ahmetchin",  19L);
        System.out.println(sqlGenerator.insert(user));
    }
}
