package services;

import dto.StudentSignUp;
import models.Student;
import org.w3c.dom.ls.LSOutput;
import repositories.StudentRepository;

public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public void signUp(StudentSignUp form) {
        Student student = Student.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .password(form.getPassword())
                .build();
        studentRepository.save(student);
    }
}
