package services;

import dto.StudentSignUp;

public interface StudentService {
    void signUp(StudentSignUp form);
}
