import jdbc.SimpleDataSource;
import models.Student;
import repositories.StudentRepository;
import repositories.StudentRepositoryJdbcTemplateImpl;
import services.StudentService;
import services.StudentServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(properties.getProperty("db.url"), properties.getProperty("db.username"), properties.getProperty("db.password"));
        StudentRepository studentRepository = new StudentRepositoryJdbcTemplateImpl(dataSource);

        Student student = Student.builder()
                .firstName("Калим")
                .lastName("Брылин")
                .email("email")
                .password("bakirbagram339")
                .build();

        studentRepository.save(student);

        Student student1 = Student.builder()
                .id(student.getId())
                .firstName("Калим")
                .lastName("Брылин")
                .email("kalim.ahmetchin")
                .password("bakirbagram339")
                .build();

        studentRepository.update(student1);

        studentRepository.delete(33L);

        System.out.println(studentRepository.findAllByAgeGreaterThanOrderByIdDesc(21));
    }
}
