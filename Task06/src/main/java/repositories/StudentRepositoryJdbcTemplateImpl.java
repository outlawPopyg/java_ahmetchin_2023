package repositories;

import models.Student;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

public class StudentRepositoryJdbcTemplateImpl implements StudentRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL_USERS = "select * from student;";

    // language=SQL
    private static final String SQL_INSERT_USER
            = "insert into student(first_name, last_name, email, password) " + "values (:firstName, :lastName, :emal, :password)";

    //language=SQL
    private static final String SQL_GET_BY_ID = "select * from student where id = ?";

    // language=SQL
    private static final String SQL_UPDATE_STUDENT =
            "update student set first_name = ?, last_name = ?, age = ?, phone_number = ?, email = ?, password = ? where id = ?;";

    // language=SQL
    private static final String SQL_DELETE_STUDENT = "delete from student where id = ?;";

    // language=SQL
    private static final String SQL_FIND_ALL_BY_AGE_GREATER =
            "select * from student where age > ? order by id desc;";

    // language=SQL
    private static final String SQL_DELETE_STUDENT_COURSE =
            "delete from student_course where student_id = ?;";

    private static final RowMapper<Student> studentMapper = (row, rowNumber) -> Student.builder() // указываем только те поля которые хотим проинициализировать
            .id(row.getLong("id"))
            .firstName(row.getString("first_name"))
            .lastName(row.getString("last_name"))
            .age(row.getObject("age", Integer.class))
            .build();

    private final JdbcTemplate jdbcTemplate;

    public StudentRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Student> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL_USERS, studentMapper);
    }

    @Override
    public Optional<Student> findById(Long id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_GET_BY_ID, studentMapper, id)); // id в качаестве вопросительного знака
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void save(Student student) {
        Map<String, Object> paramsAsMap = new HashMap<>();
        paramsAsMap.put("first_name", student.getFirstName());
        paramsAsMap.put("last_name", student.getLastName());
        paramsAsMap.put("email", student.getEmail());
        paramsAsMap.put("password", student.getPassword());
        paramsAsMap.put("phone_number", "");

        SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);

        Long id = insert.withTableName("student")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();

        student.setId(id);
    }


    @Override
    public void update(Student student) {
        jdbcTemplate.update(SQL_UPDATE_STUDENT,
                student.getFirstName(),
                student.getLastName(),
                student.getAge(),
                student.getPhoneNumber() != null ? student.getPhoneNumber() : "",
                student.getEmail(),
                student.getPassword(),
                student.getId()
        );
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update(SQL_DELETE_STUDENT_COURSE, id);
        jdbcTemplate.update(SQL_DELETE_STUDENT, id);
    }

    @Override
    public List<Student> findAllByAgeGreaterThanOrderByIdDesc(Integer minAge) {
        return jdbcTemplate.query(SQL_FIND_ALL_BY_AGE_GREATER, studentMapper, minAge);
    }
}
