package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor // конструктор со всеми параметрами
@NoArgsConstructor
@Data // геттеры, сеттеры, equals, hashCode сразу будут
@Builder
public class StudentSignUp {
    private String email;
    private String password;
    private String firstName;
    private String lastName;

}
