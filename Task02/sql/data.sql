insert into client (first_name, last_name, phone_number)
values ('Kalim', 'Ahmetchin', '89872663455'),
       ('Arthur', 'Morgan', '898723488435'),
       ('Frank', 'Vinci', '34534958'),
       ('John', 'Nicholson', '12131');

insert into driver (first_name, last_name, rating)
values ('John', 'Marston', 3),
       ('Dutch', 'Van der linde', 5),
       ('Derek', 'Frost', 4);

insert into car (model, mileage)
values ('Ford', 12000),
       ('KIA', 23455),
       ('Lada', 90881);

insert into taxi_order (address_from, address_to, price)
values ('Lenina, 20', 'Dubravnayia', 200),
       ('Prospekt Pobedi, 19', 'Lenina, 19', 650),
       ('Fuchika, 20', 'Kremlin', 1200);

update client set order_id = 1 where id = 2;
update driver set order_id = 1 where id = 3;