drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists taxi_order;

create table client
(
    id           bigserial primary key,
    first_name   char(20),
    last_name    char(20),
    phone_number char(20)
);

create table driver
(
    id         bigserial primary key,
    first_name char(20),
    last_name  char(20),
    rating float check ( rating > 0 and rating <= 5 )
);

create table car
(
    id      bigserial primary key,
    model   char(20),
    mileage integer
);

create table taxi_order
(
    id           bigserial primary key,
    address_from char(20),
    address_to   char(20),
    price        integer
);

alter table driver
    add car_id bigint;
alter table driver
    add foreign key (car_id) references car (id);

alter table client
    add order_id bigint;
alter table client
    add foreign key (order_id) references taxi_order (id);

alter table driver
    add order_id bigint;
alter table driver
    add foreign key (order_id) references taxi_order (id);